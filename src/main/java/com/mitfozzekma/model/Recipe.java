package com.mitfozzekma.model;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

@Entity
public class Recipe {

	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	private Long id;
	
	
	private String name;
	

	private String category;
	
	
	@Column(length = 8000)
	@ElementCollection
	@CollectionTable(name="INGREDIENTS", joinColumns=@JoinColumn(name="RECIPE_ID"))
	private List<String> ingredient;
	
	
	@Column(length = 8000)
	@ElementCollection
	@CollectionTable(name="DIRECTIONS", joinColumns=@JoinColumn(name="RECIPE_ID"))
	private List<String> direction;
	
	
	private String source;

	public Recipe() {}

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<String> getIngredient() {
		return ingredient;
	}

	public void setIngredient(List<String> ingredient) {
		this.ingredient = ingredient;
	}

	public List<String> getDirection() {
		return direction;
	}

	public void setDirection(List<String> direction) {
		this.direction = direction;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	
	
	public Recipe(String name, String category, List<String> ingredient, List<String> direction, String source) {
		this.name = name;
		this.category = category;
		this.ingredient = ingredient;
		this.direction = direction;
		this.source = source;
	}

	@Override
	public String toString() {
		return "Recipe [id=" + id + ", name=" + name + ", category=" + category + ", ingredients=" + ingredient
				+ ", directions=" + direction + ", source=" + source + "]";
	}
}
