package com.mitfozzekma.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mitfozzekma.model.Recipe;

@Repository
public interface RecipeRepository extends CrudRepository<Recipe, Long> {
	
	List<Recipe> findAll();
	
	List<Recipe> findByCategory(String category);
}
