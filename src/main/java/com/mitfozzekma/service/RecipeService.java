package com.mitfozzekma.service;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitfozzekma.model.Recipe;
import com.mitfozzekma.repository.RecipeRepository;

@Service
public class RecipeService {

	private RecipeRepository recipeRepo;

	@Autowired
	public void setRecipeRepo(RecipeRepository recipeRepo) {
		this.recipeRepo = recipeRepo;
	}

	public Random randomNumber = new Random();
	
	public Long getRandomRecipeId() {
		long randomRecipeNumber = randomNumber.nextInt((int) recipeRepo.count()) + 1;
		return randomRecipeNumber;
	}
	
	public Recipe getRecipe(long recipeId) {
		return recipeRepo.findOne(recipeId);
	}	
	

	public Recipe getRandomRecipe() {
		return getRecipe(getRandomRecipeId());
	}
	
	public Long getRandomCategoryId(String category) {
		int randomActualCategoryNumber = randomNumber.nextInt(recipeRepo.findByCategory(category).size());
		List<Recipe> actualCategory = recipeRepo.findByCategory(category);
		Long randomActualCategoryId = actualCategory.get(randomActualCategoryNumber).getId();
		return randomActualCategoryId;
	}
	
	public Recipe saveNewRecipe(String name, String category, String ingredient, String direction, String source) {
		
		String[] formIngredient = ingredient.split("@@ ");
		List<String> newIngredientList = Arrays.asList(formIngredient);
		   
		String[] formDirection = direction.split("@@ ");
		List<String> newDirectionList = Arrays.asList(formDirection);
		
		Recipe newRecipe = new Recipe(name, category, newIngredientList, newDirectionList, source);
		
		return recipeRepo.save(newRecipe);
	}
}