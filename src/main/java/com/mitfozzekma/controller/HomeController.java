package com.mitfozzekma.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mitfozzekma.model.Recipe;
import com.mitfozzekma.repository.RecipeRepository;
import com.mitfozzekma.service.RecipeService;

@Controller
public class HomeController {

	@Autowired
	private RecipeService recipeService;
	
	@Autowired
	private RecipeRepository recipeRepo;
	
	@RequestMapping("/")
	public String index(Model model) {
		long randomActualRandomId = recipeService.getRandomRecipeId();
		model.addAttribute("actualName", recipeService.getRecipe(randomActualRandomId).getName());
		model.addAttribute("actualId", recipeService.getRecipe(randomActualRandomId).getId());
		return "index";
	}

	@RequestMapping("/info")
	public String info(Model model) {
		return "info";
	}
	
	@RequestMapping("/ingredients/{id}")
	public String ingredients(@PathVariable(value = "id") Long id, Model model) {
		model.addAttribute("ingredientList", recipeService.getRecipe(id).getIngredient());
		model.addAttribute("actualName", recipeService.getRecipe(id).getName());
		model.addAttribute("actualId", recipeService.getRecipe(id).getId());
		model.addAttribute("actualSource", recipeService.getRecipe(id).getSource());
		return "ingredients";
	}
	
	@RequestMapping("/direction/{id}")
	public String direction(@PathVariable(value = "id") Long id, Model model) {
		model.addAttribute("directionList", recipeService.getRecipe(id).getDirection());
		model.addAttribute("actualName", recipeService.getRecipe(id).getName());
		model.addAttribute("actualId", recipeService.getRecipe(id).getId());
		model.addAttribute("actualSource", recipeService.getRecipe(id).getSource());
		return "direction";
	}
	
	@RequestMapping("/filter")
	public String filter(Model model) {
		return "filter";
	}
	
	@RequestMapping("/filter/{category}")
	public String filterCategory(@PathVariable(value = "category") String category, Model model) {
		long randomActualRandomCategoryId = recipeService.getRandomCategoryId(category);
		model.addAttribute("actualName", recipeService.getRecipe(randomActualRandomCategoryId).getName());
		model.addAttribute("actualId", recipeService.getRecipe(randomActualRandomCategoryId).getId());
		return "index";
	}
	
	@GetMapping("/add")
	public String recipeForm(Model model) {
		model.addAttribute("newRecipe", new Recipe());
		return "add";
	}
	
	@PostMapping("/add")
    public String newRecipeParams(
    		@RequestParam(name = "name") String name, 
    		@RequestParam(name = "category") String category, 
    		@RequestParam(name = "ingredient") String ingredient, 
    		@RequestParam(name = "direction") String direction, 
    		@RequestParam(name = "source") String source) {
		
		recipeService.saveNewRecipe(name, category, ingredient, direction, source);
		return "redirect:/feedback";
    }
	
	@RequestMapping("/feedback")
	public String feedback(Model model) {
		model.addAttribute("newRecipeName", recipeService.getRecipe(recipeRepo.count()).getName());
		model.addAttribute("newRecipeCategory", recipeService.getRecipe(recipeRepo.count()).getCategory());
		model.addAttribute("newRecipeIngredients", recipeService.getRecipe(recipeRepo.count()).getIngredient());
		model.addAttribute("newRecipeDirection", recipeService.getRecipe(recipeRepo.count()).getDirection());
		model.addAttribute("newRecipeSource", recipeService.getRecipe(recipeRepo.count()).getSource());
		return "feedback";
	}
}